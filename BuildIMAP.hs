{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE LambdaCase #-}

module BuildIMAP where

import Prelude hiding (FilePath)

import Control.Monad

import Data.List
import Data.Maybe


import Shelly

import qualified Data.Text as T
import qualified ShellyPathCommon as Path
import qualified ShellyBashCommon as Bash

default (T.Text)


-- Compile IMAP

yum = do
    Bash.yum_install "pam-devel"
    Bash.yum_install "openssl-devel"

download = do
    -- Source: ftp://ftp.cac.washington.edu/imap/imap.tar.gz
    run_ Path.aws ["s3", "cp", "s3://pku-api/node-init/migrate/files/imap.tar.gz", "/app/imap.tar.gz"] 
    
extract = do
    Bash.cd_root_app
    Bash.untar "imap.tar.gz"
    Bash.rm_rf "imap.tar.gz"
    Bash.mv "imap-2007f" "/usr/local/imap-2007f"

    
build = do
    cd "/usr/local/imap-2007f"
    Bash.make ["lr5", "EXTRACFLAGS=-fPIC"]

    cp_r "c-client" "lib"
    cp_r "c-client" "include"
    --cp_r c-client/c-client.a lib/libc-client.a

main :: IO ()
main = shelly $ do
    yum
    download
    extract
    build
   
