{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE LambdaCase #-}

module ConfigureMySQL where

import Prelude hiding (FilePath)

import Control.Monad

import Data.List
import Data.Maybe


import Shelly

import qualified Data.Text as T
import qualified ShellyPathCommon as Path
import qualified ShellyBashCommon as Bash
import qualified Common as C

default (T.Text)


set_password passwd = do
    run_ "mysqladmin" ["-u", "root", "password", T.pack passwd]

configure = do
    C.applyStringOperationToFileContentWithWriteWithCondition
        (C.replaceStringAwithBafterC "\n" "\nmax_allowed_packet=64M\n" "# Disabling symbolic-links is recommended to prevent assorted security risks" )
        (not . C.contains "max_allowed_packet=64M")
        "/etc/my.cnf"
 
auto_start = do
    run_ "/sbin/chkconfig" ["mysqld", "on"]
    run_ "/sbin/service" ["mysqld", "start"]


run :: String -> IO ()
run passwd = do
    configure
    shelly $ auto_start
    shelly $ set_password passwd
    




