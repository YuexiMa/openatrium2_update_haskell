{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE LambdaCase #-}

module ConfigureSite where

import Prelude hiding (FilePath)

import Control.Monad

import Data.List
import Data.Maybe


import Shelly

import qualified Data.Text as T
import qualified ShellyPathCommon as Path
import qualified ShellyBashCommon as Bash
import Common as C

import Data.Monoid ((<>), mconcat)

default (T.Text)


configure = do
    -- warning 1 solution
    C.applyStringOperationToFileContentWithWriteWithCondition
        (C.replaceStringFirst "$rendered_form = drupal_render(drupal_get_form('_cas_autologin_form', $user_name, $user_pass, $service_url));"
                            "error_reporting(E_ALL & ~E_STRICT & ~E_NOTICE);\n$rendered_form = drupal_render(drupal_get_form('_cas_autologin_form', $user_name, $user_pass, $service_url));\nerror_reporting(E_ALL);")
        (not . C.contains "error_reporting(E_ALL & ~E_STRICT & ~E_NOTICE);")
        "/app/drupal_root/sites/all/modules/cas/cas.module"

    
main :: IO ()
main = do
    configure
    




