{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE LambdaCase #-}

module ShellyBashCommon where

import Prelude hiding (FilePath)

import Control.Monad
import Data.Monoid ((<>))
import Data.Functor ((<$>))


import Data.List
import Data.Maybe


import Shelly hiding (ls, mkdir)

import qualified Data.Text as T
import qualified ShellyPathCommon as Path

default (T.Text)

ln :: T.Text -> T.Text -> Sh ()
ln a b = run_ Path.ln [a, b]

wget :: T.Text -> Sh ()
wget = run_ Path.wget . ("-t" :) . ("0" :) . (:[])

yum_update :: Sh ()
yum_update = do 
    run_ Path.yum ["-y", "update"]
    run_ Path.yum ["-y", "upgrade"]

yum_install_all :: [T.Text] -> Sh ()
yum_install_all = run_ Path.yum . ("-y" :) . ("install" :)

yum_install :: T.Text -> Sh ()
yum_install = run_ Path.yum . ("-y" :) . ("install" :) . (:[])

yum_remove :: T.Text -> Sh ()
yum_remove = run_ Path.yum . ("-y" :) . ("remove" :) . (:[])

ls :: T.Text -> Sh ()
ls = run_ Path.ls . (:[])

cd_up :: Sh()
cd_up = cd "../"

cd_root_app :: Sh()
cd_root_app = cd Path.root_app_path

tar :: [T.Text] -> Sh ()
tar params = run_ Path.tar params 

untar :: T.Text -> Sh ()
untar = run_ Path.tar . ("-xzvf" :) . (:[])

rm_rf :: T.Text -> Sh ()
rm_rf = run_ Path.rm . ("-rf" :) . (:[])

mkdirs :: FilePath -> Sh ()
mkdirs path = mkdir_p path

dot_slash_configure :: [T.Text] -> Sh ()
dot_slash_configure params = do 
    -- confPath <- (<>Path.dot_slash_configure) <$> pwd
    -- cd "."
    -- run_ Path.slash_configure params
    run_on_current_dir "/configure" params
    
-- dot_slash_configure :: [T.Text] -> Sh ()
-- dot_slash_configure params = ((<>Path.dot_slash_configure) <$> pwd)
--     >>= (\confPath -> run confPath params)


run_on_current_dir :: T.Text -> [T.Text] -> Sh ()
run_on_current_dir path params= do
    workingDir <- toTextIgnore <$> pwd
    run_ (fromText $ workingDir <> path) params


sed :: [T.Text] -> Sh ()
sed params = run_ Path.sed params

echo :: [T.Text] -> Sh ()
echo params = run_ Path.echo params

pkill :: T.Text -> Sh ()
pkill path = run_ Path.pkill [path]

mysqldump :: [T.Text] -> Sh ()
mysqldump params = run_ Path.mysqldump params

drush :: [T.Text] -> Sh ()
drush params = run_ Path.drush params

composer :: [T.Text] -> Sh ()
composer params = run_ Path.composer params

curl :: [T.Text] -> Sh ()
curl params = run_ Path.curl params

php ::  [T.Text] -> Sh ()
php params = run_ Path.php params

chmod ::  [T.Text] -> Sh ()
chmod params = run_ Path.chmod params

unzip ::  T.Text -> Sh ()
unzip param = run_ Path.unzip [param]

mv :: T.Text -> T.Text -> Sh ()
mv src dst = run_ Path.mv [src, dst]

make = do
    run_ Path.make
    
make_install = do
    run_ Path.make ["install"]