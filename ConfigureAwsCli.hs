{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE LambdaCase #-}

module ConfigureAwsCli where

import Prelude hiding (FilePath)

import Control.Monad

import Data.List
import Data.Maybe


import Shelly

import qualified Data.Text as T
import qualified ShellyPathCommon as Path
import qualified ShellyBashCommon as Bash

default (T.Text)


-- Install the AWS CLI Using the Bundled Installer http://docs.aws.amazon.com/cli/latest/userguide/installing.html

configure key skey = do
    run_ Path.aws ["configure", "set", "aws_access_key_id", T.pack key]
    run_ Path.aws ["configure", "set", "aws_secret_access_key", T.pack skey]
    run_ Path.aws ["configure", "set", "default.region", "us-east-1"]
    run_ Path.aws ["configure", "set", "default.output", "json"]

run :: String -> String -> IO ()
run key skey = shelly $ do
    configure key skey
    