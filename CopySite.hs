{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE LambdaCase #-}

module CopySite where

import Prelude hiding (FilePath)

import Control.Monad

import Data.List
import Data.Maybe


import Shelly

import qualified Data.Text as T
import qualified ShellyPathCommon as Path
import qualified ShellyBashCommon as Bash

import Data.Monoid ((<>), mconcat)

default (T.Text)


copy :: String -> Sh ()
copy username = do 
    run_ "scp" ["-r", T.pack (username <> "@10.37.49.20:/app/drupal_root/sites"), "/app/sites"]
    
move_out :: IO ()
move_out = shelly $ do
    Bash.mv "/app/drupal_root/sites" "/app/sites"
    
move_in :: IO ()
move_in = shelly $ do
    Bash.mv "/app/sites" "/app/drupal_root/sites"
    
delete_OA :: IO ()
delete_OA = shelly $ do
    Bash.rm_rf "/app/drupal_root"
    
run :: String -> IO ()
run username = shelly $ do
    copy username
    




