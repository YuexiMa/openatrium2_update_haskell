{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE LambdaCase #-}

module InstallAwsCli where

import Prelude hiding (FilePath)

import Control.Monad

import Data.List
import Data.Maybe


import Shelly

import qualified Data.Text as T
import qualified ShellyPathCommon as Path
import qualified ShellyBashCommon as Bash

default (T.Text)


-- Install the AWS CLI Using the Bundled Installer http://docs.aws.amazon.com/cli/latest/userguide/installing.html




download = do
    -- download installer
    Bash.curl ["https://s3.amazonaws.com/aws-cli/awscli-bundle.zip", "-o", "/app/awscli-bundle.zip"]

extract = do 
    -- unzip installer
    Bash.cd_root_app
    Bash.unzip "/app/awscli-bundle.zip"

install = do
    run_ "/app/awscli-bundle/install" ["-i", "/usr/local/aws", "-b", "/usr/local/bin/aws"]

cleanup = do
    Bash.rm_rf "/app/awscli-bundle"
    Bash.rm_rf "/app/awscli-bundle.zip"

main :: IO ()
main = shelly $ do
    download
    extract
    install
    cleanup
    