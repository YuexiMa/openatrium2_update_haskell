{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE LambdaCase #-}

module CopyDatabase where

import Prelude hiding (FilePath)

import Control.Monad

import Data.List
import Data.Maybe


import Shelly

import qualified Data.Text as T
import qualified ShellyPathCommon as Path
import qualified ShellyBashCommon as Bash

import Data.Monoid ((<>), mconcat)

default (T.Text)


--dump usr passwd = do 
    --mysqldump -h atrium-legacy-compat.cku7crzxi1iv.us-east-1.rds.amazonaws.com -u atrium_app_pku -p\!by8ktk+L\$4B --databases atrium | mysql -u root -patrium_passwd    


prepare :: String -> String -> String -> IO ()
prepare usr passwd sql_passwd = do
    writeFile "temp.sh" $ "mysqldump -h atrium-legacy-compat.cku7crzxi1iv.us-east-1.rds.amazonaws.com -u " <> usr <> " -p" <> passwd <> " --databases atrium > rds_dump.sql"
                           <> "\n"
                           <> "mysql -u root -p" <> sql_passwd <>" < rds_dump.sql" 
                           
dump_and_inport = do
    shelly $ run_ "bash" ["temp.sh"]

clean_up = shelly $ do
    Bash.rm_rf "temp.sh"
    Bash.rm_rf "rds_dump.sql"


run usr passwd sql_passwd = do
    prepare usr passwd sql_passwd
    print "   *** dumping production site database and importing to local..."
    dump_and_inport
    clean_up

