{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE LambdaCase #-}

module BuildApacheHttpd where

import Prelude hiding (FilePath)

import Control.Monad

import Data.List
import Data.Maybe


import Shelly

import qualified Data.Text as T
import qualified ShellyPathCommon as Path
import qualified ShellyBashCommon as Bash

default (T.Text)


-- Build Apache HTTP server http://httpd.apache.org/docs/2.4/install.html

yum_install_dependencies_httpd = do
    Bash.yum_install "gcc"

download_httpd = do
    -- download httpd 2.4.12
    Bash.wget "http://mirror.cc.columbia.edu/pub/software/apache/httpd/httpd-2.4.16.tar.gz"

extract_httpd = do
    Bash.untar "httpd-2.4.16.tar.gz"
    Bash.rm_rf "httpd-2.4.16.tar.gz"

rename_httpd = do
    Bash.rm_rf "httpd"
    mv "httpd-2.4.16" "httpd"
    
prepare_httpd = do
    yum_install_dependencies_httpd
    download_httpd
    extract_httpd
    rename_httpd
    
    
    
download_APR = do
    -- Apache APR http://apr.apache.org/download.cgi
    Bash.wget "http://www.gtlib.gatech.edu/pub/apache//apr/apr-1.5.2.tar.gz"
    
extract_APR = do
    Bash.untar "apr-1.5.2.tar.gz"
    Bash.rm_rf "apr-1.5.2.tar.gz"

move_APR = do
    Bash.rm_rf "httpd/srclib/apr"
    mv "apr-1.5.2" "httpd/srclib/apr"
    
prepare_APR = do
    download_APR
    extract_APR
    move_APR

download_APR_util = do
    Bash.wget "http://www.gtlib.gatech.edu/pub/apache//apr/apr-util-1.5.4.tar.gz"

extract_APR_util = do
    Bash.untar "apr-util-1.5.4.tar.gz"
    Bash.rm_rf "apr-util-1.5.4.tar.gz"

move_APR_util = do
    Bash.rm_rf "httpd/srclib/apr-util"
    mv "apr-util-1.5.4" "httpd/srclib/apr-util"

prepare_APR_util = do
    download_APR_util
    extract_APR_util
    move_APR_util

build_APR_util = do
    cd "httpd/srclib/apr-util/xml/expat" -- Reference: http://vova-zms.blogspot.com/2012/07/libtool-link-cannot-find-library.html
    -- Bash.ls "."
    Bash.dot_slash_configure []
    Bash.make []
    cd "../../../../../"


install_PCRE = do
    -- http://www.pcre.org/
    Bash.yum_install "pcre-devel"  -- Reference: http://forum.directadmin.com/showthread.php?t=23411

build_httpd = do
    cd "httpd"
    Bash.dot_slash_configure  ["--enable-so", "--enable-module=rewrite", "--enable-module=most"] --, "--prefix=\"/root/apache/\""] -- need APR and gcc, path may be changed if needed
    Bash.make []
    Bash.make_install
    cd "../"
    -- Bash.export "PATH=~/httpd/:$PATH"


 
remove_old = do
    Bash.yum_remove "httpd*"
    
    
    
    
main :: IO ()
main = shelly $ do
    cd "/app"
    
    remove_old
    prepare_httpd
    prepare_APR
    prepare_APR_util
    build_APR_util
    install_PCRE
    build_httpd
