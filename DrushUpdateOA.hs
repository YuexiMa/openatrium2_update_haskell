{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE LambdaCase #-}

module DrushUpdateOA where

import Prelude hiding (FilePath)

import Control.Monad

import Data.List
import Data.Maybe


import Shelly

import qualified Data.Text as T
import qualified ShellyPathCommon as Path
import qualified ShellyBashCommon as Bash

default (T.Text)


-- update OA with drush   #Reference:https://www.drupal.org/node/2321627


drush_update = do
    cd "/app/drupal_root"
    Bash.drush ["updb", "-y"]


main :: IO ()
main = shelly $ do
    drush_update
    
    
