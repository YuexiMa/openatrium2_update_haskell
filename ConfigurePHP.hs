{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE LambdaCase #-}

module ConfigurePHP where

import Prelude hiding (FilePath)

import Control.Monad

import Data.List
import Data.Maybe


import Shelly

import qualified Data.Text as T
import qualified ShellyPathCommon as Path
import qualified ShellyBashCommon as Bash
import Common as C

default (T.Text)

configure = do
    shelly $ cp "/app/php-5.6.11/php.ini-development" "/etc/php.ini"
    -- Reference: http://crybit.com/20-common-php-compilation-errors-and-fix-unix/
    
    C.replaceStringInFileAll "memory_limit = 128M" "memory_limit = 300M" "/etc/php.ini"
    C.replaceStringInFileAll "max_execution_time = 30" "max_execution_time = 60" "/etc/php.ini"
    
 
    
main :: IO ()
main = do
    configure
    
