{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

module ShellyPathCommon where

import Prelude hiding (FilePath)
import Shelly
import Data.Monoid ((<>))
import Data.Functor ((<$>))

import qualified Data.Text as T
default (T.Text)

root_app_path :: FilePath
root_app_path = "/app"

ln :: FilePath
ln = "ln"

ls :: FilePath
ls = "ls"

unzip :: FilePath
unzip = "unzip"

yum :: FilePath
yum = "yum"

wget :: FilePath
wget = "wget"

rm :: FilePath
rm = "rm"

rpm :: FilePath
rpm = "rpm"

perl :: FilePath
perl = "perl"

mv :: FilePath
mv = "mv"

mkdir :: FilePath
mkdir = "mkdir"

tar :: FilePath
tar = "tar"

make :: FilePath
make = "make"

export :: FilePath
export = "export"

slash_configure :: FilePath
slash_configure = "./configure"

sed :: FilePath
sed = "sed"

echo :: FilePath
echo = "echo"

pkill :: FilePath
pkill = "pkill"

mysqldump :: FilePath
mysqldump = "mysqldump"

drush :: FilePath
drush = "/root/.composer/vendor/bin/drush"

composer :: FilePath
composer = "/usr/local/bin/composer"

curl :: FilePath
curl = "curl"

php :: FilePath
php = "php"

chmod :: FilePath
chmod = "chmod"

aws :: FilePath
aws = "/usr/local/bin/aws"
