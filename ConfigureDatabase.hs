{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE LambdaCase #-}

module ConfigureDatabase where

import Prelude hiding (FilePath)

import Control.Monad

import Data.List
import Data.Maybe


import Shelly

import qualified Data.Text as T
import qualified ShellyPathCommon as Path
import qualified ShellyBashCommon as Bash
import qualified Common as C

import Data.Monoid ((<>), mconcat)

default (T.Text)



configure usr passwd sql_passwd = do
    C.replaceStringInFileAll 
                ("'username' => '" <> usr <> "',")
                ("'username' => 'root',")
                "/app/drupal_root/sites/default/settings.php"
    C.replaceStringInFileAll
                ("'password' => '" <> (C.replaceStringAll "\\" "" passwd) <> "',")
                ("'password' => '" <> sql_passwd <> "',")
                "/app/drupal_root/sites/default/settings.php"
    C.replaceStringInFileAll 
                ("'host' => 'atrium-legacy-compat.cku7crzxi1iv.us-east-1.rds.amazonaws.com'") 
                ("'host' => '127.0.0.1'") 
                "/app/drupal_root/sites/default/settings.php"

safety_check = do
    C.fileContentSaftyCheck "'host' => '127.0.0.1'" "/app/drupal_root/sites/default/settings.php"
              
run usr passwd sql_passwd = do
    configure usr passwd sql_passwd
    safety_check
    print "database configuration finished!"




