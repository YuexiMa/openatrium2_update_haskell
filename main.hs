{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE LambdaCase #-}

module Main where

import Prelude hiding (FilePath)

import Control.Monad
import Data.Monoid ((<>))


import Data.List
import Data.Maybe


import Shelly


-- import qualified BuildPHP

import qualified Data.Text as T

import qualified ShellyPathCommon as Path
import qualified ShellyBashCommon as Bash


import qualified BuildApacheHttpd
import qualified ConfigureApacheHttpd
import qualified InstallAwsCli
import qualified ConfigureAwsCli
import qualified BuildIMAP
import qualified BuildPHP
import qualified ConfigurePHP
import qualified InstallDrush
import qualified InstallMySQL
import qualified ConfigureMySQL
import qualified InstallOA
import qualified ConfigureOA
import qualified CopySite
import qualified CopyDatabase
import qualified ConfigureSite
import qualified ConfigureDatabase
import qualified DrushUpdateOA



default (T.Text)

preflight :: Sh [T.Text]
preflight = liftM catMaybes checks
    where checks = sequence [
           test_px Path.wget >>= (\e -> return $ if e then Nothing else Just $ "wget program not found in $PATH.")
           ,test_px Path.rpm >>= (\e -> return $ if e then Nothing else Just $ "rpm program not found in $PATH.")
           ,test_px Path.yum >>= (\e -> return $ if e then Nothing else Just $ "yum program not found in $PATH.")
           ,test_px Path.tar >>= (\e -> return $ if e then Nothing else Just $ "tar program not found in $PATH.")
           ,test_px Path.make >>= (\e -> return $ if e then Nothing else Just $ "make program not found in $PATH.")
           ,test_px Path.perl >>= (\e -> return $ if e then Nothing else Just $ "perl program not found in $PATH.")
--           ,test_f ld_local_path >>= (\e -> return $ if e then Nothing else Just $ "/etc/ld.so.conf.d/local.conf not found.")
           ]

main_shelly :: IO ()
main_shelly = (shelly) $ do
    --cd "/app/sandboxes/yma52/nodeinit_drush_OA/"
    --cmd "ls"
    --workingDir <- toTextIgnore <$> pwd
    --run_ (fromText $ workingDir<>"/test") []
    --cmd "./test"
    --echo_n "Performing pre-flight check... "
    --preflight >>= \case [] -> echo "Success."
    --                    es -> echo "Failure." >> echo (T.intercalate "\n" es) >> quietExit 1
    Bash.yum_update

main :: IO ()
main = do  

    print "Please input AWS API access key"
    awsKey <- getLine
    
    print "Please input AWS API secret access key"
    awsSKey <- getLine
    
    print "Please input a new local MySQL root password"
    mysqlpasswd <- getLine
    
    print "Please input rds username"
    rds_usr <- getLine
    
    print "Please input rds password"
    rds_passwd <- getLine
    
    print "Please input your jnj username. You will be asked for your jnj password in the next step for copying the site folder out from pku production server."
    username <- getLine
    
    
    CopySite.run username
    
    main_shelly
    
    
    BuildApacheHttpd.main
    ConfigureApacheHttpd.main
    
    InstallAwsCli.main
    ConfigureAwsCli.run awsKey awsSKey
    
    BuildIMAP.main
    
    BuildPHP.main
    ConfigurePHP.main
    
    InstallDrush.main
    
    InstallMySQL.main
    ConfigureMySQL.run mysqlpasswd
    
    InstallOA.main "2.41"
    ConfigureOA.main
    
    CopySite.move_in
    ConfigureSite.main
    
    CopyDatabase.run rds_usr rds_passwd mysqlpasswd
    ConfigureDatabase.run rds_usr rds_passwd mysqlpasswd
    
    DrushUpdateOA.main
    
    CopySite.move_out
    CopySite.delete_OA
    
    InstallOA.main "2.43"
    ConfigureOA.main
    
    CopySite.move_in
    
    DrushUpdateOA.main
    
    ConfigureApacheHttpd.apachectl_start

    print "Finished!"
    
   