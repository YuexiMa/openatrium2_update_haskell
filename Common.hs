

module Common where 

import Data.List as L (isInfixOf)

import Data.Monoid ((<>), mconcat)
import Control.Monad

import Data.Strings
import System.IO
import Control.DeepSeq

import System.Exit

fileContentSaftyCheck :: String -> FilePath -> IO ()
fileContentSaftyCheck s f = do
                h <- openFile f ReadMode
                content <- hGetContents h
                content `deepseq` hClose h
                
                if contains s content then
                    return ()
                else
                    die "file safety check failed, script stopped!"

appendStringToFile :: String -> FilePath -> IO ()
appendStringToFile s f = do
                h <- openFile f ReadMode
                content <- hGetContents h
                content `deepseq` hClose h
                
                writeFile f (content <> s)
                    
appendStringToFileIfNot :: String -> FilePath -> IO ()
appendStringToFileIfNot s f = do
                h <- openFile f ReadMode
                content <- hGetContents h
                content `deepseq` hClose h
                
                if isInfixOf s content then
                    return ()
                else
                    writeFile f (content <> s)
                    
replaceStringInFileFirst :: String -> String -> FilePath -> IO ()
replaceStringInFileFirst src dst f =
                applyStringOperationToFileContentWithWriteWithCondition (replaceStringFirst src dst) (isInfixOf src) f

                    
replaceStringInFileAll :: String -> String -> FilePath -> IO ()
replaceStringInFileAll src dst f = 
                applyStringOperationToFileContentWithWriteWithCondition (replaceStringAll src dst) (isInfixOf src) f

                    
                    
contains :: String -> String -> Bool
contains a s = isInfixOf a s 

applyStringOperationToFileContentWithWriteWithCondition :: (String -> String) -> (String -> Bool) -> FilePath -> IO ()
applyStringOperationToFileContentWithWriteWithCondition opr cond f = do
                h <- openFile f ReadMode
                content <- hGetContents h
                content `deepseq` hClose h
                
                if cond content then
                    writeFile f (opr content)
                else
                    return ()
                    
applyStringOperationToFileContentWithWrite :: (String -> String) -> FilePath -> IO ()
applyStringOperationToFileContentWithWrite opr f = do
                h <- openFile f ReadMode
                content <- hGetContents h
                content `deepseq` hClose h
                
                writeFile f (opr content)
                    
replaceStringAwithBafterC :: String -> String -> String -> String -> String
replaceStringAwithBafterC a b c s = replaceStringAwithBafterC_helper (strSplit c s) a b c s

replaceStringAwithBafterC_helper :: (String, String) -> String -> String-> String -> String -> String 
replaceStringAwithBafterC_helper ( head, "") _ _ _ s = s
replaceStringAwithBafterC_helper ( head, tail) a b c s = head <> c <> (replaceStringFirst a b tail)

       


                    
replaceStringFirst :: String -> String -> String -> String
replaceStringFirst src dst s = replaceStringFirst_helper (strSplit src s) dst
                
replaceStringFirst_helper :: (String, String) -> String -> String
replaceStringFirst_helper ( head, "") dst = head
replaceStringFirst_helper ( head, tail) dst = head <> dst <> tail


                    
replaceStringAll :: String -> String -> String -> String
replaceStringAll src dst s = replaceStringAll_helper (strSplit src s) src dst
                          
replaceStringAll_helper :: (String, String) -> String -> String -> String
replaceStringAll_helper ( head, "") src dst = head
replaceStringAll_helper ( head, tail) src dst = head <> dst <> (replaceStringAll_helper (strSplit src tail) src dst)
