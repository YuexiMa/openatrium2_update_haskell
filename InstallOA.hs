{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE LambdaCase #-}

module InstallOA where

import Prelude hiding (FilePath)

import Control.Monad

import Data.List
import Data.Maybe


import Shelly

import qualified Data.Text as T
import qualified ShellyPathCommon as Path
import qualified ShellyBashCommon as Bash

import Data.Monoid ((<>), mconcat)

default (T.Text)


    
main :: String -> IO ()
main version = shelly $ do
    Bash.cd_root_app
    download version
    extract version
    clean_up version
    move version





download version = do
    Bash.wget (T.pack $ "http://ftp.drupal.org/files/projects/openatrium-7.x-" <> version <> "-core.tar.gz")

extract version = do
    Bash.untar (T.pack $ "openatrium-7.x-" <> version <> "-core.tar.gz")

clean_up version = do
    Bash.rm_rf (T.pack $ "openatrium-7.x-" <> version <> "-core.tar.gz")
    Bash.rm_rf (T.pack $ "openatrium-7.x-" <> version <> "/sites")
    
move version = do
    Bash.mv (T.pack $ "openatrium-7.x-" <> version <> "/") "/app/drupal_root"