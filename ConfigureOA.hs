{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE LambdaCase #-}

module ConfigureOA where

import Prelude hiding (FilePath)

import Control.Monad

import Data.List
import Data.Maybe


import Shelly

import qualified Data.Text as T
import qualified ShellyPathCommon as Path
import qualified ShellyBashCommon as Bash
import qualified Common as C

default (T.Text)


    
main :: IO ()
main = do
    configure
    


configure = do
    C.applyStringOperationToFileContentWithWriteWithCondition
        (C.replaceStringAwithBafterC "\n" "\n  php_value upload_max_filesize             500M\n" "<IfModule mod_php5.c>" )
        (not . C.contains "php_value upload_max_filesize             500M")
        "/app/drupal_root/.htaccess"
    C.applyStringOperationToFileContentWithWriteWithCondition
        (C.replaceStringAwithBafterC "\n" "\n  php_value post_max_size                   500M\n" "<IfModule mod_php5.c>" )
        (not . C.contains "php_value post_max_size                   500M")
        "/app/drupal_root/.htaccess"
    C.applyStringOperationToFileContentWithWriteWithCondition
        (C.replaceStringAwithBafterC "\n" "\n  php_value memory_limit                    300M\n" "<IfModule mod_php5.c>" )
        (not . C.contains "php_value memory_limit                    300M")
        "/app/drupal_root/.htaccess"
 
    -- Error 5 solution
    -- install and start mysql
    
    -- Error 2 solution   --ONLY for OA 2.41-2.42
    C.replaceStringInFileAll "'subject' => substr($node->title, 0, 64)" "'subject' => mb_substr($node->title, 0, 64)" "/app/drupal_root/profiles/openatrium/modules/apps/oa_discussion/oa_discussion.install"

    -- Error 4 solution
    C.applyStringOperationToFileContentWithWriteWithCondition
        (C.replaceStringAwithBafterC "\n" "\n  RewriteBase /\n" "RewriteEngine on" )
        (not . C.contains "RewriteBase /")
        "/app/drupal_root/.htaccess"
 

    C.replaceStringInFileAll "$cid .= '_' . (!empty($arg) ? $arg : '');" "$cid .= '_' . serialize(!empty($arg) ? $arg : '');" "/app/drupal_root/profiles/openatrium/modules/contrib/oa_core/includes/oa_core.cache.inc"
    -- Error: notice
    --      Notice: Array to string conversion in oa_core_get_cache_id() (line 69 of /app/drupal_root/profiles/openatrium/modules/contrib/oa_core/includes/oa_core.cache.inc).
