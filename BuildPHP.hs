{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE LambdaCase #-}

module BuildPHP where

import Prelude hiding (FilePath)

import Control.Monad

import Data.List
import Data.Maybe


import Shelly

import qualified Data.Text as T
import qualified ShellyPathCommon as Path
import qualified ShellyBashCommon as Bash

default (T.Text)


yum_install_dependencies = do
    Bash.yum_install "gcc"
    Bash.yum_install "libxml2-devel"
    Bash.yum_install "libcurl-devel"
    Bash.yum_install "openssl-devel"
    Bash.yum_install "libjpeg-devel"
    Bash.yum_install "libpng-devel"
    Bash.yum_install "gmp-devel"
    
download = do
    -- php5.6.10 // may have 2 test fail
    Bash.wget "http://php.net/distributions/php-5.6.11.tar.gz"
    
extract = do
    Bash.untar "php-5.6.11.tar.gz"
    Bash.rm_rf "php-5.6.11.tar.gz"


make = do
    cd "php-5.6.11"
    Bash.dot_slash_configure  ["--with-apxs2=/usr/local/apache2/bin/apxs",  "--with-openssl",  "--with-mysql",  "--with-mysqli",  "--with-gd",  "--with-curl",  "--with-pear",  "--with-gmp",  "--with-pdo-mysql",  "--enable-ftp", "--enable-mbstring", "--enable-fpm", "--with-zlib", "--enable-zip", "--with-imap=/usr/local/imap-2007f", "--with-imap-ssl"]
    Bash.make []
    Bash.make_install


remove_old = do
    Bash.yum_remove "php*"
    
main :: IO ()
main = shelly $ do
    cd "/app"
    
    remove_old
    yum_install_dependencies
    download
    extract
    make
    