{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE LambdaCase #-}

module InstallMySQL where

import Prelude hiding (FilePath)

import Control.Monad

import Data.List
import Data.Maybe


import Shelly

import qualified Data.Text as T
import qualified ShellyPathCommon as Path
import qualified ShellyBashCommon as Bash

default (T.Text)


yum = do
    Bash.yum_install "mysql"
    Bash.yum_install "mysql-server"


    
main :: IO ()
main = shelly$ do
    yum
    




