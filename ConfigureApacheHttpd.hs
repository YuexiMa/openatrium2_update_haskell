{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE LambdaCase #-}

module ConfigureApacheHttpd where

import Prelude hiding (FilePath)

import Control.Monad

import Data.List
import Data.Maybe


import Shelly

import qualified Data.Text as T
import qualified ShellyPathCommon as Path
import qualified ShellyBashCommon as Bash
import Common as C

default (T.Text)


configure_httpd :: IO ()
configure_httpd = do
    -- Reference: http://php.net/manual/en/install.unix.apache2.php
    C.replaceStringInFileAll "/usr/local/apache2/htdocs" "/app/drupal_root"  "/usr/local/apache2/conf/httpd.conf"
    C.replaceStringInFileAll "/usr/local/apache2//htdocs" "/app/drupal_root"  "/usr/local/apache2/conf/httpd.conf"
    
    -- automatically load index.php
    C.replaceStringInFileAll "DirectoryIndex index.html" "DirectoryIndex index.php index.html"  "/usr/local/apache2/conf/httpd.conf"
    
    
    -- error 7 solution
    C.replaceStringInFileAll "#LoadModule rewrite_module" "LoadModule rewrite_module"  "/usr/local/apache2/conf/httpd.conf"
    
    C.appendStringToFileIfNot "\napachectl start" "/etc/rc.local"
    
    C.applyStringOperationToFileContentWithWriteWithCondition (replaceStringAwithBafterC " AllowOverride None" "AllowOverride All" "/app/drupal_root" ) (isInfixOf "/app/drupal_root")  "/usr/local/apache2/conf/httpd.conf"

append_php_handler :: IO ()
append_php_handler = do
    -- Reference: http://php.net/manual/en/install.unix.apache2.php
    appendStringToFileIfNot "\n<FilesMatch \\.php$>\n    SetHandler application/x-httpd-php\n</FilesMatch>\n" "/usr/local/apache2/conf/httpd.conf"
    appendStringToFileIfNot "<FilesMatch \"\\.phps$\">\n    SetHandler application/x-httpd-php-source\n</FilesMatch>\n" "/usr/local/apache2/conf/httpd.conf" 

addpath :: IO ()
addpath = shelly $ do
            Bash.ln "/usr/local/apache2/bin/apachectl" "/usr/bin/apachectl"
            
apachectl_start :: IO ()
apachectl_start = shelly $ do
            run_ "apachectl" ["start"]
            
main :: IO ()
main = do 
    configure_httpd
    append_php_handler
    addpath