{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE LambdaCase #-}

module InstallDrush where

import Prelude hiding (FilePath)

import Control.Monad

import Data.List
import Data.Maybe


import Shelly

import qualified Data.Text as T
import qualified ShellyPathCommon as Path
import qualified ShellyBashCommon as Bash

default (T.Text)


-- install drush stable

-- require php version > 5.3.3 installed
-- Reference: http://simpleinformation.com/blog/step-step-instructions-install-new-drush-drush-7-required-drupal-8

install_composer = do
    Bash.cd_root_app
    Bash.curl ["-sS", "https://getcomposer.org/installer", "-o", "composer.installer"]
    --writeFile "composer.installer" content
    Bash.php ["composer.installer"]
    Bash.mv "composer.phar" "/usr/local/bin/composer"
    Bash.rm_rf "composer.installer"

install_drush = do
    Bash.composer ["global", "require", "drush/drush:7.*"]

add_path = do
    --export PATH="$HOME/.composer/vendor/bin:$PATH"
    writeFile "/etc/profile.d/drush_path.sh" "export PATH=$HOME/.composer/vendor/bin:$PATH"
    --source ~/.bash_profile 
    
    
main :: IO ()
main = do
    shelly $ install_composer
    shelly $ install_drush
    add_path
    




